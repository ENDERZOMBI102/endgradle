import java.io.InputStream
import java.net.URL
import java.security.MessageDigest

plugins {
	kotlin("jvm") version "1.7.20"
	`java-gradle-plugin`
	`maven-publish`
}

repositories {
	mavenCentral()
	maven( url="https://repo.sleeping.town" )
	maven( url="https://repsy.io/mvn/enderzombi102/mc" )
	maven( url="https://maven.quiltmc.org/repository/release" )
}


dependencies {
	implementation( "me.obsilabor:piston-meta-kt:1.0.6" )
	implementation( "com.enderzombi102:EnderLib:0.3.2" )
	implementation( "com.unascribed:flexver-java:1.0.2" )

	// TODO: make some dependencies optional
	// deserialization libs
	implementation( "org.tomlj:tomlj:1.1.0" )
	implementation( "blue.endless:jankson:1.2.1" )

	implementation( "org.quiltmc:quiltflower:1.9.0" )
	implementation( "commons-codec:commons-codec:1.15" )

	// test deps
	testImplementation( kotlin("script-runtime") )
	testImplementation( "org.junit.jupiter:junit-jupiter" )
	testImplementation( platform( "org.junit:junit-bom:5.9.1" ) )
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
	kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Test> {
	useJUnitPlatform()
	testLogging.showStandardStreams = true
}

tasks.withType<Jar> {
	manifest.attributes( "Implementation-Version" to archiveVersion )
}

java.withSourcesJar()

gradlePlugin.plugins.create("enderman") {
	id = "com.enderzombi102.enderman"
	displayName = "enderman"
	description = "A plugin to help in minecraft mod development and some general gradle QoL stuff"
	implementationClass = "com.enderzombi102.enderman.EndermanRootPlugin"
}

publishing.repositories {
	maven {
		name = "Repsy"
		credentials( PasswordCredentials::class )
		url = uri("https://repsy.io/mvn/enderzombi102/mc")
	}
	maven {
		name = "TestMaven"
		url = buildDir.resolve("repo").also(File::mkdirs).toURI()
	}
}

val webgen by tasks.registering( JavaExec::class ) {
	// task meta
	description = "Generate website from static markdown."
	group = "documentation"

	// the task's classpath is the webgen subproject's one, so build it first
	val webgen = project("webgen")
	dependsOn( webgen.tasks.classes )
	classpath = webgen.sourceSets.main.get().runtimeClasspath

	// kolin adds `Kt` postfix to no-class files
	mainClass.set("com.enderzombi102.webgen.WebgenKt")
	args = listOf( "-o", "public", "docs" )
}

val downloadGradleSources by tasks.registering {
	// constants
	val url = "https://services.gradle.org/distributions/gradle-${gradle.gradleVersion}-src.zip"
	val sourcesJar = gradle.gradleUserHomeDir.resolve("caches/${gradle.gradleVersion}/generated-gradle-jars/gradle-api-${gradle.gradleVersion}-sources.jar")

	// meta
	group = "plugin development"
	description = "Downloads the gradle source jar."

	// declare task I/O
	inputs.property( "gradleVersion", gradle::getGradleVersion )
	outputs.file( sourcesJar )

	doLast {
		// if it doesn't exist, we can't be up-to-date
		if ( sourcesJar.exists() ) {
			// get the sha256 of the online file
			val onlineSha = String( URL( "$url.sha256" ).openStream().use( InputStream::readAllBytes ) )
			// calculate the sha256 of the local file
			val localSha = MessageDigest.getInstance("SHA-256")
				.digest( sourcesJar.readBytes() )
				.fold("") { str, byte -> str + "%02x".format( byte ) }

			// if up-to-date, do nothing
			if ( onlineSha == localSha ) {
				println("Up to date, nothing to do.")
				return@doLast
			}

			println("File is corrupted, will download again.")
			sourcesJar.delete()
		}

		println("Downloading $url to $sourcesJar")
		sourcesJar.writeBytes( URL(url).openStream().use( InputStream::readAllBytes ) ) // download the zip
	}
}
