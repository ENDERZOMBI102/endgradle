plugins {
	kotlin("jvm") version "1.7.20"
}

group = "com.enderzombi102"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
	implementation( "org.jetbrains:markdown:0.3.4" )
}
