package com.enderzombi102.webgen

class ArgvHelper( private val argv: MutableList<String> ) {
	fun flag( flag: String ): Boolean {
		for ( ( index, arg ) in argv.iterator().withIndex() ) {
			if ( arg == flag ) {
				argv.removeAt( index )
				return true
			}
		}
		return false
	}

	fun option( flag: String ): String? {
		for ( ( index, arg ) in argv.iterator().withIndex() ){
			if ( arg == flag ) {
				argv.removeAt( index )
				return argv.removeAt( index )
			}
		}
		return null
	}
	fun optionMany(flag: String ): List<String> = buildList {
		var index = 0

		while ( index < argv.size ) {
			if ( argv[index] == flag ) {
				argv.removeAt( index )
				add( argv.removeAt( index ) )
			} else
				index += 1
		}
	}

	fun positional(): String = argv.removeAt(0)
	fun remaining(): List<String> = argv
}
