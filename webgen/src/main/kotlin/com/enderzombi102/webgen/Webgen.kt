@file:OptIn(ExperimentalPathApi::class)

package com.enderzombi102.webgen

import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.*

fun main( argv: Array<String> ) {
	val start = System.nanoTime()
	val argvHelper = ArgvHelper( argv.toMutableList() )

	val threaded = argvHelper.flag( "--threaded" )
	val output = Path( argvHelper.option( "-o" ) ?: "out" )
	val input = Path( argvHelper.positional() )

	val flavor = CommonMarkFlavourDescriptor()
	val parser = MarkdownParser( flavor )

	fun process( file: Path ) {
		val src = file.readText()
		val ast = parser.buildMarkdownTreeFromString( src )
		output
			.resolve( input.relativize( file.parent ) )
			.also { it.createDirectories() }
			.resolve( file.toFile().nameWithoutExtension + ".html" )
			.writeText( HtmlGenerator( src, ast, flavor ).generateHtml() )
	}

	Files.walkFileTree(
		input,
		fileVisitor {
			onVisitFile { file, attributes ->
				if (! attributes.isDirectory ) {
					if ( threaded )
						Thread { process( file ) }.start()
					else
						process( file )
				}
				FileVisitResult.CONTINUE
			}
		}
	)

	println("Finished in ${System.nanoTime() - start}ns")
}
