pluginManagement {
	repositories {
		mavenCentral()
		gradlePluginPortal()
		maven( url = "https://repsy.io/mvn/enderzombi102/mc" )
	}
}

rootProject.name = "enderman"

include("webgen")
