enderman
-
A gradle plugin to simplify minecraft development

you can find docs on the [wiki](https://gitlab.com/ENDERZOMBI102/enderman/-/wikis/home) and (later) on the [pages site](https://enderzombi102.gitlab.io/enderman)

build.gradle.kts
```kotlin
plugins {
    id( "com.enderzombi102.enderman" ) version "0.1.1"
}

enderman {
    // applies a configuration template, parameter is either a URL or a string which will be passed to `project.file()`
    applyTemplate( "https://gitlab.com/ENDERZOMBI102/enderman/-/raw/master/templates/quiltmc.emt.toml" )
    
    useGlobalCaching = true // every dependency will be cached globally based on mappings and version
    runConfigs {
        named("client") {
            // client configuration
            classpath = sourceSets.main.get().runtimeClasspath
            genIdeConfig = true  // force-generate the IDE run config
        }
        named("server") {
            // server configuration
            classpath = sourceSets.main.get().runtimeClasspath
            genIdeConfig = true  // force-generate the IDE run config
        }
    }
}

val quiltMappings: String by enderman
dependencies {
    // declaring the minecraft dependency + remapping it
    implementation( enderman.mapped( "com.mojang:minecraft:1.19.2", quiltMappings ) ) // uses specified mappings if provided
    // or
    mappedImplementation( "com.mojang:minecraft:1.19.2" ) // uses global mappings
    // or
    implementation( enderman.mapped {
        mappings( quiltMappings ) // uses specified mappings if provided
        input( "com.mojang:minecraft:1.19.2" ) {
            isGloballyCached = true // always globally cache
        }
    })
    // download a file directly from an url, you can specify classifiers to download multiple
    implementation( enderman.download( "https://build.lwjgl.org/addons/lwjglx-debug/lwjglx-debug-1.0.0.jar", "-sources", "-javadoc" ) )
}

tasks.withType<JavaCompile> {
    
}
```


settings.gradle.kts
```kotlin
pluginManagement {
    repositories {
        mavenCentral()
        maven( url="https://repo.sleeping.town" ) // will be removed in the future
        maven( url="https://repsy.io/mvn/enderzombi102/mc" )
        maven( url="https://maven.quiltmc.org/repository/release" ) // will be removed in the future
    }
}
```
