import blue.endless.jankson.Jankson
import blue.endless.jankson.JsonArray
import blue.endless.jankson.JsonObject
import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DynamicNode
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.io.CleanupMode
import org.junit.jupiter.api.io.TempDir
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode
import java.io.File
import java.io.IOException
import java.net.URL
import kotlin.reflect.cast

@Execution(ExecutionMode.CONCURRENT)
class EndermanPluginTest {
	@TestFactory
	@Suppress("UNCHECKED_CAST")
	fun factory(): Iterable<DynamicNode> =
		Jankson.builder().build().load( File( resource("tests.json5")!!.toURI() ) )
			.map { ( name, value ) ->
				println( "generating test data for $name" )
				dynamicTest( name, resource( name )!!.toURI() ) {
					val obj = ( value as JsonObject )
					val fail = obj.getBoolean( "fail", false )
					val args = obj.get( List::class.java, "args" ) as List<String>? ?: listOf()
					val checks = obj.get( JsonArray::class.java, "checks" )!!
						.map { Check( it as JsonObject ) }
					genericTest( name, fail, checks, args )
				}
			}

	/**
	 * A generic test implementation
	 */
	private fun genericTest( name: String, fail: Boolean, checks: List<Check>, args: List<String> = emptyList() ) {
		val folder = setupTestFiles( name, testProjectDir )
		val runner = GradleRunner.create()
			.withTestKitDir( File( folder.parentFile, "testkit" ) )
			.withProjectDir( folder )
			.withPluginClasspath()
			.withArguments( args )
		val result = if ( fail ) runner.buildAndFail() else runner.build()

		for ( ( index, check ) in checks.withIndex() ) {
			val ( message, value, type ) = check
			when ( type ) {
				Check.Type.RegexMatch -> assertTrue(
					result.output.matches( value.toRegex() ),
					"Regex check at #$index failed: $message"
				)
				Check.Type.RegexMatchInverted -> assertFalse(
					result.output.matches( value.toRegex() ),
					"Inverted regex check at #$index failed: $message"
				)
				Check.Type.Contains -> assertTrue(
					result.output.contains( value ),
					"Substring check at #$index failed: $message"
				)
				Check.Type.ContainsInverted -> assertFalse(
					result.output.contains( value ),
					"Inverted substring check at #$index failed: $message"
				)
			}
		}
	}

	/**
	 * Setups the files for the test
	 */
	private fun setupTestFiles( name: String, folder: File ): File {
		// create test project folder
		val projDir = File( folder, name ).also( File::mkdir )
		println( "Running test `$name` in `${projDir.toString().replace( System.getProperty("user.home"), "~" )}`" )

		// copy resources
		resource("$name/settings.gradle.kts")?.also {
			File( projDir, "settings.gradle.kts" ).writeText( it.readText() )
		}
		resource("$name/gradle.properties")?.also {
			File( projDir, "gradle.properties" ).writeText( it.readText() )
		}
		resource("$name/build.gradle.kts")?.also {
			File( projDir, "build.gradle.kts" ).writeText( it.readText() )
		}
		resource("$name/test.emt.toml")?.also {
			File( projDir, "test.emt.toml" ).writeText( it.readText() )
		}

		// return project folder path
		return projDir
	}

	/**
	 * Returns the URL of a resource
	 */
	private fun resource( path: String ): URL? =
		EndermanPluginTest::class.java.getResource("/test/$path")

	companion object {
		@BeforeAll
		@JvmStatic
		fun setup( @TempDir( cleanup = CleanupMode.NEVER ) file: File ) {
			testProjectDir = file
		}

		lateinit var testProjectDir: File
	}

	class Check( obj: JsonObject ) {
		private val message: String = obj.get( String::class.java, "message" ) ?: ""
		private val value: String = obj.get( String::class.java, "value" )!!
		private val type: Type = Type.values().first { it.code == ( obj.get( String::class.java, "type" ) ?: "in" ) }

		operator fun component1() = message
		operator fun component2() = value
		operator fun component3() = type

		enum class Type( val code: String ) {
			RegexMatch("match"),
			RegexMatchInverted("!match"),
			Contains("in"),
			ContainsInverted("!in")
		}
	}
}
