import org.gradle.api.artifacts.repositories.MavenArtifactRepository
plugins {
	id( "com.enderzombi102.enderman" ) version "0.1.1"
}

enderman.applyTemplate( file( "test.emt.toml" ).toURI().toString() )

repositories.forEach { println( "${ it.name } -> ${ ( it as MavenArtifactRepository).url }" ) }

