@file:Suppress("UnstableApiUsage", "VulnerableLibrariesLocal")
import org.gradle.api.artifacts.repositories.MavenArtifactRepository.MetadataSources

plugins {
	id( "com.enderzombi102.enderman" )
}

repositories {
	maven( url="https://maven.minecraftforge.net" ) {
		metadataSources( MetadataSources::artifact ) // required for forge artifact ( no POM.xml )
	}
	maven( url="https://libraries.minecraft.net" )
    mavenCentral()
}

val clientUrl = "https://launcher.mojang.com/v1/objects/e80d9b3bf5085002218d4be59e668bac718abbc6/client.jar"

dependencies {
	"decompile"( implementation( enderman.download( clientUrl ) )!! )
	"decompile"( implementation( "net.minecraftforge:forge:1.7.10-10.13.4.1614-1.7.10:universal" )!! )

	"natives"( "net.java.jinput:jinput-platform:2.0.5:natives-windows" )
	"natives"( "org.lwjgl.lwjgl:lwjgl-platform:2.9.4-nightly-20150209:natives-windows" )
	"natives"( "tv.twitch:twitch-platform:5.16:natives-windows-64" )
	"natives"( "tv.twitch:twitch-external-platform:4.5:natives-windows-64" )
}


