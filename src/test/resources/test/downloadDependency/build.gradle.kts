import java.io.File

plugins {
	id( "com.enderzombi102.enderman" )
}

val lwjglDebug = "https://build.lwjgl.org/addons/lwjglx-debug/lwjglx-debug-1.0.0.jar"

configurations
	.detachedConfiguration( enderman.download( lwjglDebug, "-sources", "-javadoc" ) )
	.files
	.also { it.map( File::exists ).also( ::println ) }
	.map { it.relativeTo( projectDir ) }
	.also( ::println )

