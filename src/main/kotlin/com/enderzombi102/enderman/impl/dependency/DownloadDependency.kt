package com.enderzombi102.enderman.impl.dependency

import com.enderzombi102.enderman.impl.Utility.filename
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.component.ComponentIdentifier
import org.gradle.api.internal.artifacts.dependencies.SelfResolvingDependencyInternal
import org.gradle.api.tasks.TaskDependency
import java.io.File
import java.io.InputStream
import java.net.URL

class DownloadDependency( project: Project, private val urls: List<URL> ) : EndermanDependency( project ), SelfResolvingDependencyInternal {
	override fun getGroup(): String? =
		urls[0].host

	override fun getName(): String =
		urls[0].file.substringAfterLast("/")

	override fun getVersion(): String? =
		null

	override fun contentEquals( dependency: Dependency ): Boolean =
		dependency is DownloadDependency && dependency.urls == urls

	override fun copy(): Dependency =
		DownloadDependency( project, urls ).also { it.because( reason ) }
	
	override fun getBuildDependencies(): TaskDependency =
		TaskDependency { setOf() }

	override fun resolve(): MutableSet<File> =
		urls.map { url ->
				val file = project.file( ".gradle/repo" ).also { it.mkdirs() }.resolve( url.filename )

				if (! file.exists() ) {
					project.logger.info( "File `${url.filename}` is missing, downloading..." )
					file.writeBytes( url.openStream().use( InputStream::readAllBytes ) )
				}

				file
			}
			.toMutableSet()

	override fun getTargetComponentId(): ComponentIdentifier =
		ComponentIdentifier { toString() }
}
