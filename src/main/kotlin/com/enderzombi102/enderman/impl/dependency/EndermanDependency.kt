package com.enderzombi102.enderman.impl.dependency

import org.gradle.api.Project
import org.gradle.api.artifacts.FileCollectionDependency
import org.gradle.api.file.FileCollection
import java.io.File

abstract class EndermanDependency( protected val project: Project ) : FileCollectionDependency {
	private var reason: String? = null

	override fun getReason(): String? =
		reason

	override fun because( reason: String? ) {
		this.reason = reason
	}

	override fun resolve( transitive: Boolean ): MutableSet<File> =
		resolve()

	override fun getFiles(): FileCollection =
		project.files( resolve() )
}
