package com.enderzombi102.enderman.impl.flower

import org.gradle.api.logging.Logger
import org.jetbrains.java.decompiler.main.extern.IFernflowerLogger

class GradleFernflowerLogger(private val logger: Logger) : IFernflowerLogger() {
	override fun writeMessage( message: String, severity: Severity ) = when ( severity ) {
		Severity.TRACE -> logger.trace( message )
		Severity.INFO -> logger.info( message )
		Severity.WARN -> logger.warn( message )
		Severity.ERROR -> logger.error( message )
	}

	override fun writeMessage( message: String, severity: Severity, t: Throwable ) = when ( severity ) {
		Severity.TRACE -> logger.trace( message, t )
		Severity.INFO -> logger.info( message, t )
		Severity.WARN -> logger.warn( message, t )
		Severity.ERROR -> logger.error( message, t )
	}
}
