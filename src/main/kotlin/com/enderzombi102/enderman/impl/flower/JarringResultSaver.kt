package com.enderzombi102.enderman.impl.flower

import org.gradle.api.logging.Logger
import org.jetbrains.java.decompiler.main.DecompilerContext
import org.jetbrains.java.decompiler.main.extern.IFernflowerPreferences
import org.jetbrains.java.decompiler.main.extern.IResultSaver
import org.jetbrains.java.decompiler.util.InterpreterUtil
import org.jetbrains.java.decompiler.util.ZipFileCache
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.jar.JarOutputStream
import java.util.jar.Manifest
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class JarringResultSaver( private val root: File, private val logger: Logger ) : IResultSaver {
	private val mapArchiveStreams = HashMap<String, ZipOutputStream>()
	private val mapArchiveEntries: MutableMap<String, MutableSet<String>> = HashMap()
	private val openZips = ZipFileCache()

	init { require( root.isDirectory ) { "Provided path '$root' must be a directory" } }

	override fun saveFolder( path: String ) {
		val dir = File( path.getAbsolutePath() )
		if (! ( dir.mkdirs() || dir.isDirectory ) )
			throw RuntimeException("Cannot create directory $dir")
	}

	override fun copyFile( source: String, path: String, entryName: String ) {
		try {
			InterpreterUtil.copyFile( File( source ), File( path.getAbsolutePath(), entryName ) )
		} catch ( ex: IOException ) {
			logger.error( "Cannot copy $source to $entryName", ex )
		}
	}

	override fun saveClassFile( path: String, qualifiedName: String?, entryName: String, content: String?, mapping: IntArray? ) {
		val file: File = File( path.getAbsolutePath(), entryName )
		if ( content != null ) {
			try {
				file.writeText( content )
			} catch ( ex: IOException ) {
				logger.error( "Cannot write class file $file", ex )
			}
		} else
			logger.warn( "Attempted to write null class file to $file" )
	}

	override fun createArchive( path: String, archiveName: String, manifest: Manifest? ) {
		val file = File( path.getAbsolutePath(), archiveName.sourcify() )
		try {
			if (! ( file.createNewFile() || file.isFile ) )
				throw IOException("Cannot create file $file")

			val fileStream = FileOutputStream(file)
			val zipStream = manifest?.let { JarOutputStream( fileStream, it ) } ?: ZipOutputStream( fileStream )
			mapArchiveStreams[file.path] = zipStream
		} catch ( ex: IOException ) {
			logger.error( "Cannot create archive $file", ex )
		}
	}

	override fun saveDirEntry( path: String, archiveName: String, entryName: String ) =
		saveClassEntry(
			path,
			archiveName,
			null,
			if (! entryName.endsWith("/") )
				"$entryName/"
			else
				entryName,
			null
		)

	override fun saveClassEntry( path: String, archiveName: String, qualifiedName: String?, entryName: String, content: String? ) =
		this.saveClassEntry( path, archiveName, qualifiedName, entryName, content, null )

	override fun saveClassEntry( path: String, archiveName: String, qualifiedName: String?, entryName: String, content: String?, mapping: IntArray? ) {
		val file: String = File( path.getAbsolutePath(), archiveName.sourcify() ).getPath()

		if (! checkEntry( entryName, file ) )
			return

		try {
			val out: ZipOutputStream? = mapArchiveStreams[file]
			val entry = ZipEntry(entryName)

			if ( mapping != null && DecompilerContext.getOption( IFernflowerPreferences.DUMP_CODE_LINES ) )
				entry.extra = getCodeLineData( mapping )

			out!!.putNextEntry( entry )

			if ( content != null )
				out.write( content.toByteArray( StandardCharsets.UTF_8 ) )
		} catch ( ex: IOException ) {
			logger.error( "Cannot write entry $entryName to $file", ex )
		}
	}

	override fun copyEntry( source: String, path: String, archiveName: String, entryName: String ) {
		val file: String = File( path.getAbsolutePath(), archiveName.sourcify() ).getPath()

		if (! checkEntry( entryName, file ) )
			return

		try {
			val srcArchive = openZips[source]
			val entry = srcArchive.getEntry( entryName )
			if ( entry != null ) {
				srcArchive.getInputStream( entry ).use { stream ->
					stream.transferTo( mapArchiveStreams[file]!!.also { it.putNextEntry( ZipEntry( entryName ) ) } )
				}
			}
		} catch ( ex: IOException ) {
			logger.error( "Cannot copy entry $entryName from $source to $file", ex )
		}
	}

	override fun closeArchive( path: String, archiveName: String ) {
		val file: String = File( path.getAbsolutePath(), archiveName.sourcify() ).path
		try {
			mapArchiveEntries.remove(file)
			mapArchiveStreams.remove(file)!!.close()
		} catch ( ex: IOException ) {
			logger.warn( "Cannot close $file" )
		}
	}

	override fun close(): Unit =
		openZips.close()

	private fun checkEntry( entryName: String, file: String ): Boolean {
		val set = mapArchiveEntries.computeIfAbsent( file ) { HashSet() }
		val added = set.add( entryName )
		if ( !added && !entryName.endsWith("/") )
			logger.warn( "Zip entry $entryName already exists in $file" )
		return added
	}

	private fun String.getAbsolutePath(): String =
		File( root, this ).getAbsolutePath()

	private fun String.sourcify(): String =
		split(".").run { filter { it != last() }.joinToString(".") + "-sources." + last() }
}
