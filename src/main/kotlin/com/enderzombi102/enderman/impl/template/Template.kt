package com.enderzombi102.enderman.impl.template

data class Template( val repositories: List<Repository>, val mappings: List<Mappings> )
data class Repository( val repositories: List<Repository> )
data class Mappings( val name: String,  )
data class Mapping( val format: MappingFormat )


