package com.enderzombi102.enderman.impl.template

import com.enderzombi102.enderman.impl.Utility.factory
import com.enderzombi102.enderman.impl.mapping.MappingManager
import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.tomlj.Toml
import org.tomlj.TomlArray
import org.tomlj.TomlTable
import java.net.URI
import java.net.URL


object TemplateManager {
	private val templates: MutableMap<String, TomlTable> = HashMap()

	private fun load( url: String ): TomlTable {
		if ( url in templates )
			return templates[ url ]!!

		@Suppress("HttpUrlsUsage") // this is dumb, thanks IDEA
		val templateData = when {
			url.startsWith("https://") || url.startsWith("file:/") -> URL( url ).openStream().readBytes().decodeToString()
			url.startsWith("http://") -> throw UnsupportedOperationException( "Cannot apply template from unsecure URL `$url`." )
			else -> throw IllegalStateException( "Invalid input: `$url`." )
		}
		val template = Toml.parse( templateData )

		if ( template.hasErrors() )
			throw Throwable( "Failed to load template from `$url`." ).also {
				template.errors().forEach( it::addSuppressed )
			}

		templates[ url ] = template
		return template
	}

	fun apply( project: Project, url: String ) {
		// name of the template
		val template = load( url )

		project.applyRepositories( url, template.getArrayOrEmpty( "repositories" ) )

		project.applyMappings( url, template.getArrayOrEmpty( "mappings" ) )
	}

	private fun Project.applyRepositories( url: String, repos: TomlArray ) {
		for ( index in 0 until repos.size() ) {
			val line = repos.inputPositionOf( index ).line()
			if ( repos[ index ] !is TomlTable )
				error( "Invalid repository declaration in template at `$url` in line $line: entry should be a table." )

			val repo = repos.getTable( index )

			val name = repo.getString( "name" ) ?:
				error( "Invalid repository declaration in template at `$url` in line $line: repo name is missing." )
			val maven = repo.getString( "maven" ) ?:
				error( "Invalid repository declaration in template at `$url` in line $line: repo maven url is missing." )

			// skip repo if it was added manually
			if ( repositories.find { it is MavenArtifactRepository && it.url.toString() == maven } != null ) {
				logger.warn( "Skipping configuration template repo `$name` as it is already defined." )
				continue
			}

			// skip repo if it was added manually
			if ( repositories.find { it.name == name } != null )
				logger.warn( "There already is a repository named `$name`." )

			repositories += repositories.factory.createMavenRepository().also {
				it.name = name
				it.url = URI( maven )
			}
		}
	}

	private fun Project.applyMappings( url: String, arrayOrEmpty: TomlArray ) {
		val mappings = MappingManager.getOrCreate(this)


	}
}

