package com.enderzombi102.enderman.impl.attribute

import org.gradle.api.attributes.Attribute

object Attributes {
	@JvmStatic
	val mappingState: Attribute<String> = Attribute.of( "enderman.mappingState", String::class.java )
}
