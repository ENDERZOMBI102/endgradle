package com.enderzombi102.enderman.impl

import com.enderzombi102.enderlib.reflection.Getters
import com.enderzombi102.enderman.api.EndermanExtension
import com.enderzombi102.enderman.impl.Utility.ext
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.UnknownDomainObjectException
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.internal.artifacts.BaseRepositoryFactory
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.api.tasks.SourceSetContainer
import org.tomlj.TomlArray
import java.net.URL

internal object Utility {
	internal val Project.sourceSets: SourceSetContainer
		inline get() = extensions["sourceSets"] as SourceSetContainer

	internal val Project.extension: EndermanExtension
		inline get() = extensions[ "enderman" ] as EndermanExtension

	internal val URL.filename: String
		inline get() = path.substringAfterLast("/").substringBefore('?')

	internal val RepositoryHandler.factory: BaseRepositoryFactory
		inline get() = Getters.get( this, "repositoryFactory", BaseRepositoryFactory::class.java )

	internal val Project.ext: ExtraPropertiesExtension
		inline get() = extensions.getByName( "ext" ) as ExtraPropertiesExtension


	internal val Project.paths: EndermanPaths
		inline get() = if ( hasAttachment<EndermanPaths>() ) attachment<EndermanPaths>() else attach( EndermanPaths( this ) )


	@Throws(UnknownDomainObjectException::class)
	internal operator fun <T> NamedDomainObjectContainer<T>.get( key: String ): T =
		getByName( key )

	@Throws(UnknownDomainObjectException::class)
	internal operator fun ExtensionContainer.get( name: String ): Any =
		getByName( name )

	internal operator fun <T : ExtensionAware> T.invoke( block: T.() -> Unit ) {
		block.invoke( this )
	}

	internal fun dbg( message: String ) {
		val frame = Thread.currentThread().stackTrace[2]
		println( "${frame.fileName}:${frame.lineNumber}: $message" )
	}

	/**
	 * Attach it to this project.
	 * @param T: the type of the attachment.
	 */
	internal inline fun <reified T> Project.attach( value: T ): T {
		ext[ "enderman.impl.${ T::class.simpleName!!.replaceFirstChar( Char::lowercase ) }" ] = value

		return value
	}

	/**
	 * Checks if this project has an attachment of a given type.
	 * @param T: the type of the attachment.
	 */
	internal inline fun <reified T> Project.hasAttachment(): Boolean =
		ext.has( "enderman.impl.${ T::class.simpleName!!.replaceFirstChar( Char::lowercase ) }" )

	/**
	 * Gets an object which was attached to this project.
	 * @param T: the type of the attachment.
	 */
	internal inline fun <reified T> Project.attachment(): T {
		val key = "enderman.impl.${ T::class.simpleName!!.replaceFirstChar( Char::lowercase ) }"

		if ( ext.has( key ) ) {
			val value = ext[ key ]
			if ( value is T )
				return value
			error( "Failed to get `$key` attachment as ${ T::class.qualifiedName }" )
		}

		error( "No attachment named $name!" )
	}
}
