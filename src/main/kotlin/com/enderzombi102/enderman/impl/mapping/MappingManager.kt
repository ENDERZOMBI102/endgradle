package com.enderzombi102.enderman.impl.mapping

import com.enderzombi102.enderman.impl.Utility.attach
import com.enderzombi102.enderman.impl.Utility.attachment
import com.enderzombi102.enderman.impl.Utility.hasAttachment
import org.gradle.api.Project

class MappingManager( private val project: Project ) {



	companion object {
		fun getOrCreate( project: Project ): MappingManager =
			if ( project.hasAttachment<MappingManager>() )
				project.attachment()
			else
				project.attach( MappingManager( project ) )
	}
}
