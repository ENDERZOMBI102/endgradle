package com.enderzombi102.enderman.impl

import com.enderzombi102.enderman.impl.Utility.extension
import org.gradle.api.Project
import java.io.File

class EndermanPaths( private val project: Project ) {
	/**
	 * The folder where enderman stores all its data
	 */
	val endermanDir: File
		get() = if ( project.extension.useGlobalCaching.get() ) project.gradle.gradleUserHomeDir.resolve( "enderman" ) else project.rootProject.file( ".gradle/enderman" )

	/**
	 * The folder where extracted natives should be stored.
	 */
	val nativesDir: File
		get() = endermanDir.resolve( "natives" )

	/**
	 * The folder where extracted natives should be stored.
	 */
	val assetsDir: File
		get() = endermanDir.resolve( "assets" )

}
