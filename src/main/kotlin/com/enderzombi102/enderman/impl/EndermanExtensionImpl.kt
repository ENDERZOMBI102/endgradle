package com.enderzombi102.enderman.impl

import com.enderzombi102.enderman.api.EndermanExtension
import com.enderzombi102.enderman.impl.dependency.DownloadDependency
import com.enderzombi102.enderman.impl.template.TemplateManager
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import java.net.URL

abstract class EndermanExtensionImpl( private val project: Project ) : EndermanExtension {
	override fun download( url: String, vararg classifiers: String ): Dependency =
		DownloadDependency(
			project,
			listOf( "", *classifiers )
				.map { url.substringBeforeLast(".") + "$it.jar" }
				.map( ::URL )
		)

	override fun applyTemplate( url: String ) =
		TemplateManager.apply( project, url )
}
