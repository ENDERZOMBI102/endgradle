package com.enderzombi102.enderman.impl

import com.enderzombi102.enderman.api.EndermanExtension
import com.enderzombi102.enderman.impl.Utility.ext
import com.enderzombi102.enderman.impl.Utility.extension
import com.enderzombi102.enderman.impl.Utility.get
import com.enderzombi102.enderman.impl.Utility.paths
import com.enderzombi102.enderman.impl.attribute.Attributes
import com.enderzombi102.enderman.impl.attribute.RemappingTransformer
import com.enderzombi102.enderman.impl.task.DownloadAssetsTask
import com.enderzombi102.enderman.impl.task.GenSourcesTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.io.InputStream
import java.nio.file.Files
import java.util.zip.ZipFile

class EndermanPlugin : Plugin<Project> {
	override fun apply( target: Project ) {
		target.logger.lifecycle( "enderman: ${ javaClass.`package`.implementationVersion ?: "0.1.1+dev" }" )

		// default plugins
		target.pluginManager.apply( "java" )
		target.pluginManager.apply( "idea" )

		// extensions
		val extension = target.extensions.create(
			EndermanExtension::class.java,
			"enderman",
			EndermanExtensionImpl::class.java,
			target
		)

		target.setupConfigurations()
		target.setupTasks()
	}

	private fun Project.setupConfigurations() {
		dependencies.attributesSchema.attribute( Attributes.mappingState )

		val implementation = configurations["implementation"]
		implementation.attributes.attribute( Attributes.mappingState, "mapped" )

		configurations.register( "mapImplementation" ) {
			implementation.extendsFrom( it )
			it.attributes.attribute( Attributes.mappingState, "obfuscated" )
		}

		dependencies.registerTransform( RemappingTransformer::class.java ) { spec ->
			spec.from.attribute( Attributes.mappingState, "obfuscated" )
			spec.to.attribute( Attributes.mappingState, "mapped" )
		}

		configurations.register("decompile")
		configurations.register("natives") {
			implementation.extendsFrom( it )
		}
	}

	private fun Project.setupTasks() {
		tasks.register( "downloadAssets", DownloadAssetsTask::class.java )
		tasks.register( "genSources", GenSourcesTask::class.java )

		tasks.register("extractNatives") {
			it.group = "other"
			it.description = "Extracts the natives so they can be loaded by the jvm."

			it.doFirst {
				configurations["natives"].files.forEach { jar ->
					val zip = ZipFile( jar )
					zip.entries().asIterator().forEach { entry ->
						if ( "META-INF" !in entry.name ) {
							val filePath = paths
								.nativesDir
								.also { path -> path.mkdirs() }
								.resolve( entry.name )
							if (! entry.isDirectory )
								Files.write( filePath.toPath(), zip.getInputStream( entry ).use( InputStream::readAllBytes ) )
							else
								filePath.mkdirs()
						}
					}
				}
			}
		}
	}
}
