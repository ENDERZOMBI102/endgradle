package com.enderzombi102.enderman.impl.task

import com.enderzombi102.enderman.impl.flower.GradleFernflowerLogger
import com.enderzombi102.enderman.impl.flower.JarringResultSaver
import com.enderzombi102.enderman.impl.Utility.sourceSets
import org.gradle.api.DefaultTask
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.TaskAction
import org.jetbrains.java.decompiler.main.Fernflower
import org.jetbrains.java.decompiler.main.extern.IFernflowerPreferences
import com.enderzombi102.enderman.impl.Utility.get

open class GenSourcesTask : DefaultTask() {
	private val decompile: Configuration = project.configurations["decompile"]

	init {
		group = "run"
		description = "Generate sources for the environment."

		for ( file in decompile.files )
			outputs.file( temporaryDir.resolve( "${file.nameWithoutExtension}-sources.jar" ) )
	}

	@TaskAction
	fun genSources() {
		val fernflower = Fernflower(
			JarringResultSaver( project.file(".gradle/sources").also { it.mkdirs() }, logger ),
			mapOf(
				IFernflowerPreferences.THREADS to "${Runtime.getRuntime().availableProcessors()}",
				IFernflowerPreferences.LOG_LEVEL to "trace",
				IFernflowerPreferences.INDENT_STRING to "\t",
				IFernflowerPreferences.HIDE_EMPTY_SUPER to "0",
				IFernflowerPreferences.SWITCH_EXPRESSIONS to "0",
//				IFernflowerPreferences.INCLUDE_JAVA_RUNTIME to project.extension.java8dir.get(),
				IFernflowerPreferences.BYTECODE_SOURCE_MAPPING to "1",
				IFernflowerPreferences.HIDE_DEFAULT_CONSTRUCTOR to "0",
				IFernflowerPreferences.DECOMPILE_GENERIC_SIGNATURES to "1"
			),
			GradleFernflowerLogger( logger )
		)
		try {
			decompile.files.forEach( fernflower::addSource )
			project.sourceSets["main"].runtimeClasspath.asFileTree.forEach( fernflower::addLibrary )
			fernflower.decompileContext()
		} finally {
			fernflower.clearContext()
		}
	}
}
