package com.enderzombi102.enderman.impl.task

import blue.endless.jankson.Jankson
import blue.endless.jankson.JsonObject
import com.enderzombi102.enderman.impl.Utility.extension
import com.enderzombi102.enderman.impl.Utility.paths
import org.apache.commons.codec.binary.Hex
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.InputStream
import java.net.URL
import java.security.MessageDigest


open class DownloadAssetsTask : DefaultTask() {
	private val jankson = Jankson.builder().build()
	// TODO: Move to constructor via detection of mc dependency
	private val indexUrl = "https://launchermeta.mojang.com/v1/packages/1863782e33ce7b584fc45b037325a1964e095d3e/1.7.10.json"
	private val sha1 = MessageDigest.getInstance("SHA-1")

	init {
		group = "other"
		description = "Downloads all the assets for the game."

		// do not the download if all is here
		outputs.upToDateWhen {
			jankson.load( URL(indexUrl).openStream() )
				.getObject("objects")!!
				.keys
				.all { project.paths.assetsDir.resolve(it).exists() }
		}
	}

	@TaskAction
	fun downloadAssets() {
		val assetsDir = project.paths.assetsDir

		jankson.load( URL( indexUrl ).openStream() )
			.getObject("objects")!!
			.entries
			.parallelStream()
			.forEach { ( filename, value ) ->
				val res = value as JsonObject
				val hash = res.get( String::class.java, "hash" )
				val size = res.getInt( "size", -1 )
				hash!!
				assert( size != -1 )

				// cache validation!
				if ( assetsDir.resolve( filename ).exists() && digest( assetsDir.resolve( filename ).readBytes() ) == hash )
					return@forEach logger.info( "skipping {}, already downloaded", filename )

				try {
					logger.info( "downloading `{}`..", filename )
					var array: ByteArray
					// download again and again until it is downloaded successfully
					do
						array = URL("https://resources.download.minecraft.net/${hash.substring(0, 2)}/$hash")
							.openStream()
							.use( InputStream::readAllBytes )
					while ( array.size != size && digest( array ) != hash )

					// write the downloaded data to disk
					assetsDir.resolve( filename ).also { it.parentFile.mkdirs(); it.createNewFile() }.writeBytes( array )
					logger.debug( "`{}` final status: completed", filename )
				} catch ( e: Exception ) {
					logger.error( "`{}` final status: failed", filename, e )
				}
			}
	}

	private fun digest( array: ByteArray ): String =
		Hex.encodeHexString( sha1.also { it.update( array ) }.digest() )
}
