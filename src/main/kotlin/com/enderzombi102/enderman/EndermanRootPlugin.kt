package com.enderzombi102.enderman

import com.enderzombi102.enderman.impl.EndermanPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.PluginAware

class EndermanRootPlugin : Plugin<PluginAware> {
	override fun apply( target: PluginAware ) {
		when ( target ) {
			is Project -> target.plugins.apply( EndermanPlugin::class.java )
		}
	}
}
