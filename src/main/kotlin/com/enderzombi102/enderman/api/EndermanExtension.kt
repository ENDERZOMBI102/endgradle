package com.enderzombi102.enderman.api

import org.gradle.api.artifacts.Dependency
import org.gradle.api.provider.Property

interface EndermanExtension {
	/**
	 * Whether to use the global or workspace-specific cache
	 */
	val useGlobalCaching: Property<Boolean>

	/**
	 * Download a dependency directly from an url.
	 */
	fun download( url: String, vararg classifiers: String = arrayOf( "" ) ): Dependency

	/**
	 * Applies a configuration template to the current project.
	 *
	 * @param url an url pointing to a file encoded in UTF-8.
	 */
	fun applyTemplate( url: String )
}
